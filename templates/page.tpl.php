<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>

    
    <!-- Begin Header -->
<header class="row readers-header">
        


        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="col-lg-9 col-sm-7 hidden-xs logo">
               
                <a href="/"><img src="<?php print $GLOBALS['base_url'] ?>/sites/all/themes/strivingreaders/images/readers-logo.png" alt="Striving Readers Logo" class="img-responsive" /></a>
				
                <div class="logo-text">
                    <h1>Striving Readers</h1>
                </div>
            </div>

            <div class="col-lg-3 col-sm-5 hidden-xs">
              <!--  <form class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="go">Go</button>
                </form> -->
           	  <div class="text-nowrap">   <?php 
				if($page['search']):
					print render($page['search']); 
				endif;
			    ?>
				</div>
            </div>
            

            
        </div>

        <nav class="navbar col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="navbar-header visible-xs col-xs-12">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
        <a class="navbar-brand visible-xs" href="#"><img src="/strivingreaders/sites/all/themes/strivingreaders/images/readers-logo.png" alt="Striving Readers Logo" /></a>
            </div>
   <?php print(render($page['main_navigation'])); ?>

        </nav>
    </header>

<!-- End Header -->
 
       <!--BEGIN CONTENT WRAPPER-->
    <div id="readers-wrap">
        <div class="row">
            <div class="readers-content col-lg-12 col-xs-12">

                <section class="head-image">
                    
               <?php 
				if($page['banner']):
					print render($page['banner']); 
				endif;
			?>
                </section>

                <section class="bg-blue center home-section-pad">
                    <div class="container">
                        <h2>                          <?php if ($title): ?>
                           <?php print $title; ?>
                        <?php endif; ?></h2>
                    </div>
                </section>


                 
                
                <section class="internal-page-content">
					<div class="container">
						<div class="col-lg-12 col-xs-12">
								 <?php print render($page['content']); ?>
						</div>				
					</div>
				</section>

                
                
         
              <?php 
				if($page['featured_third']):              

					print render($page['<section class="home-bottom-image">' . 'featured_third' . '</section>'  ]);
				endif; 
			?>
                                                  
                
                
                
                


    <!--END CONTENT WRAPPER -->   
   
   <!--BEGIN CONTENT WRAPPER-->
  <div id="strivingreaders-wrap">
  	<div class="row">
        <div class="strivingreaders-content col-lg-12 col-xs-12">
        

            
            <?php 
				if($page['page_heading']):
					print render($page['page_heading']);
				endif; 
			?>
            
            <?php 
				if($page['sidebar_first']):
					print render($page['sidebar_first']);
				endif; 
			?>
            
            <?php //if ($breadcrumb): ?>
             <!-- <div id="breadcrumb"><?php // print $breadcrumb; ?></div> -->
            <?php // endif; ?>
            
            <?php print $messages; ?>
            
            <div class="container">
                <div class="col-lg-12 col-xs-12">
                    <?php if ($page['highlighted']): ?>
                        <div id="highlighted"><?php print render($page['highlighted']); ?></div>
                    <?php endif; ?>
                    <a id="main-content"></a>
                    <?php print render($title_prefix); ?>
                        <?php if ($title): ?>
                           <!-- <h1 class="title" id="page-title"><?php print $title; ?></h1> -->
                        <?php endif; ?>
                    <?php print render($title_suffix); ?>
                    
                    <?php if ($tabs): ?>
                        <div class="tabs">
                            <?php print render($tabs); ?>
                        </div>
                    <?php endif; ?>
                
                    <?php print render($page['help']); ?>
                    
                    <?php if ($action_links): ?>
                        <ul class="action-links">
                            <?php print render($action_links); ?>
                        </ul>
                    <?php endif; ?>
                
                    <?php //print $feed_icons; ?>
              </div> <!-- /div -->
          </div> <!-- /.container -->
            

<section class="home-section-pad">
                <div class="container">
            <?php 
				if($page['front_blockone']):
				print t('<div class="col-lg-12 col-xs-12">
                        <div class="col-lg-3 col-xs-12">
                            <div class="bg-light-green box">
                                <img src="/images/nam-icon.png" alt="FAQs Icon" />
								                                <h1>FAQs</h1>');
					print render($page['front_blockone']);
				print t('<a href="/oela/faqs">View <span class="fa fa-chevron-right"></span></a>
                            </div>
                        </div>');	
				endif; 
			?> 
            
            <?php 
				if($page['front_blocktwo']):
				print t('<div class="col-lg-3 col-xs-12">
                            <div class="bg-blue-green box">
                                <img src="/images/npd-icon.png" alt="Spreadsheets Icon" />
                                <h1>Characteristic Spreadsheets</h1>');
					print render($page['front_blocktwo']);
				print t('<a href="/oela/content/characteristics">View <span class="fa fa-chevron-right"></span></a>
                            </div>
                        </div>');	
				endif; 
			?> 
            

            
            <?php 
				if($page['front_blockfour']):
				print t('<div class="col-lg-3 col-xs-12">
                            <div class="bg-slate box">
                                <img src="/images/logic-icon.png" alt="" />
                                <h1>Logic Model</h1>');
					print render($page['front_blockfour']);
				print t('<a href="/oela/content/logic-model-form">View <span class="fa fa-chevron-right"></span></a>
                            </div>
                        </div>');	
				endif; 
			?>                                      
                        
                </div>
         </section>
			
                       
            <?php 
				if($page['sidebar_second']):
					print render($page['sidebar_second']);
				endif; 
			?>

	<?php  if (user_is_logged_in()): ?>            
            <?php if($page['featured_first']): ?>
                <section class="home-features home-documents">
                    <div class="header-block"></div>
                        <div class="container">
                            <h2>RECENTLY ADDED DOCUMENTS</h2>
                            <div class="text-block col-lg-12 col-xs-12">
                                <div class="col-lg-7 col-sm-6 col-xs-12">
                                    <p>To view, upload, share and download updated and archived documents, Lorem ipsum dolor sit amet, laoreet dolor nec nulla tempus, pellentesque id, cras suscipit pellentesque turpis neque lobortis luctus, magnis nonummy per qui ut phasellus, varius dui leo semper consectetuer tortor conubia. Luctus id neque sed sit urna accumsan, odio convallis luctus parturient ut risus.</p>
                                </div>
                                <div class="col-lg-1 hidden-sm hidden-xs"></div>
                                <div class="col-lg-4 col-sm-6 col-xs-12">
                                    <?php print render($page['featured_first']); ?>
                                </div>
                        </div>
                    </div>
                </section>
                
            <?php endif; ?>
            
            <?php if($page['featured_second']): ?>
                <section class="home-features home-forums">
                    <div class="header-block"></div>
                    <div class="container">
                        <h2>SoP FORUMS</h2>
                        <div class="text-block col-lg-12 col-xs-12">
                            <div class="col-lg-7 col-sm-6 col-xs-12">
                                <p>To view, upload, share and download updated and archived documents, Lorem ipsum dolor sit amet, laoreet dolor nec nulla tempus, pellentesque id, cras suscipit pellentesque turpis neque lobortis luctus, magnis nonummy per qui ut phasellus, varius dui leo semper consectetuer tortor conubia. Luctus id neque sed sit urna accumsan, odio convallis luctus parturient ut risus.</p>
                            </div>
                            <div class="col-lg-1 hidden-sm hidden-xs"></div>
                            <div class="col-lg-4 col-sm-6 col-xs-12">
                                <?php print render($page['featured_second']); ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
           <?php endif; ?>  
            

            
            
        </div>
    </div>
  </div>
  <!--END CONTENT WRAPPER -->
  
  
    
    <!-- Start Footer -->
 
    <footer class="row readers-footer">
        <div class="container">
        <div class="footer-nav">
        	<ul>
	
       <?php print render($page['footer']); ?>
       <!-- footer <li>s go here -->
			</ul>
            <div class="text-center">
            <img src="<?php print $GLOBALS['base_url'] ?>/sites/all/themes/strivingreaders/images/ed-logo-white_0.png" class="img-responsive text-center ed-logo" alt="ED Logo"/>
            
            </div>
        </div>
		</div>
    </footer>
<!-- End Footer -->
            </div><!-- End readers-content -->
         </div>
    </div> <!-- End readers-wrap -->