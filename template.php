<?php

/***********************************************************************************************************************
 * *********************************************************************************************************************
 *                                      OVERRIDE THE MAIN MENU LINKS GENERATION
 * *********************************************************************************************************************
/***********************************************************************************************************************/

function strivingreaders_menu_tree($variables) {
	//if(preg_match('', $variables['tree']);
  	return '<ul class="nav navbar-nav center testingxxx">' . $variables['tree'] . '</ul>';
}

// Customize to include Bootstrap like navigation menus

function strivingreaders_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
	
  if ($element['#below']) {
	
	$element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
	$element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
	
	$sub_menu = drupal_render($element['#below']);
  }
  
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  
  //Append class='active' to the link element
  if ( $element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page()) ) {
    $element['#attributes']['class'][] = 'active';
  }
  if($element['#below']){
	  $element['#attributes']['class'][] = 'dropdown';
  }
  
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}


function strivingreaders_image($variables) {
	
  $attributes = $variables['attributes'];
  $attributes['src'] = file_create_url($variables['path']);
  $attributes['class'] = 'img-responsive';

  foreach (array('width', 'height', 'alt', 'title') as $key) {

    if (isset($variables[$key])) {
      $attributes[$key] = $variables[$key];
    }
  }

  return '<img' . drupal_attributes($attributes) . ' />';
}

function strivingreaders_preprocess_views_view(&$vars){

    //kpr($vars);

}

function strivingreaders_form_alter(&$form, &$form_state, $form_id) {

    if(isset($form_id) && $form_id == 'search_block_form'){

        $form['#attributes'] = array('class' => array('navbar-form', 'navbar-right'));

        if(!user_is_logged_in()){
            $form['login'] = array(
                '#type'         =>  'item',
                '#markup'       =>  "<a href=\"#\" data-toggle=\"modal\" data-target=\"#modal-login\"><span class=\"loginLabel\">" . t('OELA Login') . "</span></a>",
                '#weight'       => -1

            );
        }


        if($form['search_block_form']['#type'] == 'textfield'){
            $form['search_block_form']['#attributes']['placeholder'] = 'Search';
            $form['search_block_form']['#attributes']['class'][] = 'form-control';
            $form['search_block_form']['#size'] = 26;
            $form['actions']['submit']['#value'] = t('GO');
            $form['actions']['submit']['#attributes']['class'][] = 'go';
        }

    }

}

function  strivingreaders_preprocess_views_view_table(&$vars){
	
  $view = $vars['view'];
  $vars['classes_array'][] = 'table table-striped table-bordered table-hover'; // add boostrap styling ODW
  $query = db_query('SELECT data FROM {webform_submitted_data} where cid=1');
  $records = $query->fetchAll();
  $grantIDs = array();

  foreach($records as $record){
    $grantIDs[] = $record->data;
  }

  foreach ($view->result as $key=>$row) {
    
    if(isset($row->taxonomy_term_data_name)){
    if(in_array($row->taxonomy_term_data_name, $grantIDs)){
      unset($view->result[$key]);
      unset($vars['rows'][$key]);
    }
  }
  }

  
  //kpr($vars);

}
/**
* hook_form_FORM_ID_alter
*/
function strivingreaders_form_search_block_form_alter(&$form, &$form_state, $form_id) {
    $form['search_block_form']['#title'] = t('Search'); // Change the text on the label element
    $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
    // $form['search_block_form']['#size'] = 30;  // define size of the textfield
    $form['search_block_form']['#default_value'] = t('Search'); // Set a default value for the textfield
    $form['actions']['submit']['#value'] = t('GO'); // Change the text on the submit button
   // $form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/images/search-button.png');

    // Add extra attributes to the text box
    $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Search';}";
    $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'Search') {this.value = '';}";
    // Prevent user from searching the default text
    $form['#attributes']['onsubmit'] = "if(this.search_block_form.value=='Search'){ alert('Please enter a search'); return false; }";

    // Alternative (HTML5) placeholder attribute instead of using the javascript
    $form['search_block_form']['#attributes']['placeholder'] = t('Search');
} 